# Pools, vibes solo transcription

## Description

This is a transcription of Mike MAINIERI's vibes solo from "Pools" he played
along with Steps Ahead in a video concert from 1983, in Copenhagen's Carlsberg
Glyptotek.

## Download PDF

Use the following link to download the score from this repository in PDF format.

https://gitlab.com/sigmate/transcription-pools-vibes-solo/raw/master/score.pdf

## Videos

Watch "Pools" on YouTube:

https://youtu.be/PN8SAAMG8_o?t=7m12s

Or directly jump to the vibes solo:

https://youtu.be/PN8SAAMG8_o?t=11m25s

## License

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License.

## Execution notes

Notes in parenthesis are some kind of ghost notes MAINIERI actually played but
which are barely audible. These should be played as quietly as possible.

Notes with an asterisk on the bottom are (in my humble opinion) obviously
missed strokes. The note indicated in parenthesis is a suggestion of the note
that was intended.

## To do

- add dynamics
- possibly more articulations and slurs
- better differenciation and duration with acciaccaturas and appoggiaturas
- possibly more missed stroke to suggest correction with.

## Distribution & feedback

Anyone is encouraged to share this work as long as it is done respectfully to
the terms of its license. Feedback is also greatly appreciated and can be sent
to contact@mathieudemange.fr.

## Source code & contributions

This score was typesetted with LilyPond 2.19.30 and its source code can be
modified, redistributed (see license), etc. Any contribution is welcome!

## Dedication

This transcription is dedicated to William CAROSELLA & Jean-Michel DAVIS